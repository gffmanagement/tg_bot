#! /usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import json
from time import sleep

from telebot import apihelper
from multiprocessing import Process

import config
import telebot
import requests

import keyboards
import models

apihelper.proxy = config.TG_PROXY
bot = telebot.TeleBot(config.TG_TOKEN)

# available_currencies_url = 'http://127.0.0.1:8898/api/0.0.1/currencies/crypto'
available_currencies_url = 'http://u988348y.beget.tech/api/0.0.1/currencies/crypto'
# currencies_prices_url = 'http://127.0.0.1:8898/api/0.0.1/currencies/prices/crypto?from={}&to={}'
currencies_prices_url = 'http://u988348y.beget.tech/api/0.0.1/currencies/prices/crypto?from={}&to={}'

# Заголовки для запроса курсов
headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
}

def get_available_currencies():
    """
    Получает список всех доступных криптовалют
    :return: список валют
    """
    response = requests.get(available_currencies_url, headers=headers).json()
    return response['currencies']


@bot.message_handler(commands=['start', 'configure'])
def configure_bot(message):
    """
    Настройка бота
    1 шаг выбор основной валюты
    :param message:
    :return:
    """

    # Получаем список криптовалют
    crypto_s = get_available_currencies()
    # Получаем запись о пользователе из базы данных, если пользователь не существует, создаем его
    user, created = models.User.get_or_create(chat_id=message.chat.id)
    # Получаем основную валюту пользователя
    currency = user.base_currency
    # Создаем Inline клавиатуру
    markup = keyboards.keyboard_inline_radio_select_crypto(
        currencies=crypto_s,
        base_currency=currency
    )
    # Отправляем сообщение
    msg = bot.send_message(
        message.chat.id,
        text='Выберите основную валюту, в которой хотите получать цены на другие валюты',
        reply_markup=markup
    )
    # Регистрируем следующий шаг
    bot.register_next_step_handler(msg, process_following_currencies)


@bot.message_handler(commands=['followed'])
def followed(message):
    # Получаем запись о пользователе из базы данных, если пользователь не существует, создаем его
    user, created = models.User.get_or_create(chat_id=message.chat.id)
    # Поолучаем список валют которые выбрал пользователь
    followed_currencies = json.loads(user.followed_currencies)
    # Проверяем, если список выбранных валют пуст то отправляем сообщение с необходимостью выбрать вылюты
    if not len(followed_currencies):
        msg_text = 'Для просмотра списка выбранных валют их сначала ' \
                   'нужно выбрать, нажмите /configure чтобы сделать это'
        bot.send_message(
            chat_id=message.chat.id,
            text=msg_text,
        )
    else:
        msg_text = 'Список валют на которые вы подписались, нажав на кнопку вы отпишетесь от валюты'
        markup = keyboards.keyboard_inline_checkbox_select_crypto(user_followed=followed_currencies)
        # Отправляем сообщение с текстом и клавиатурой
        bot.send_message(
            chat_id=message.chat.id,
            text=msg_text,
            reply_markup=markup
        )


@bot.message_handler(commands=['rates'])
def rates(message):
    # Получаем запись о пользователе из базы данных, если пользователь не существует, создаем его
    user, created = models.User.get_or_create(chat_id=message.chat.id)
    # Поолучаем список валют которые выбрал пользователь
    to_list = json.loads(user.followed_currencies)
    # Скливаем их в строку через запятую
    to = ','.join(to_list)
    # Получаем основную валюту пользователя
    _from = user.base_currency
    # Проверяем, если список выбранных валют пуст то отправляем сообщение с необходимостью выбрать вылюты
    if not len(to_list):
        msg_text = 'Для полученя курсов валют нужно выбрать их из списка, для настройки нажмите /configure'
    else:
        # Если список валют не пуст то отправляем запрос на получение цен, преобразуем ответ в JSON
        response = requests.get(currencies_prices_url.format(_from, to), headers=headers).json()
        msg_text = ''
        # В цикле формируем сообщение пользователю с ценами валют
        for price in response:
            msg_text += ('Цена за 1 {}: *{:.8f}* {}\n'.format(price['from'], float(price['price']), price['to']))
    # Отправляем сообщение с результатом
    bot.send_message(message.chat.id, msg_text, parse_mode='MARKDOWN')


def process_following_currencies(message):
    # Получаем список доступных валют
    crypto_s = get_available_currencies()
    # Получаем запись о пользователе из базы данных, если пользователь не существует, создаем его
    user, created = models.User.get_or_create(chat_id=message.chat.id)
    # Поолучаем список валют которые выбрал пользователь
    followed_currencies = json.loads(user.followed_currencies)
    # Создаем Inline клавиатуру
    markup = keyboards.keyboard_inline_checkbox_select_crypto(
        currencies=crypto_s,
        user_followed=followed_currencies
    )

    # Текст сообщение к которму будет прикреплена коавиатура
    msg_text = 'Выберите валюты по котрым хотите получать курс\n' \
               'По окончанию нажмите кнопку ️🗄 Завершить'

    # Отправляем сообщение с текстом и клавиатурой
    bot.send_message(
        chat_id=message.chat.id,
        text=msg_text,
        reply_markup=markup
    )


@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    if 'follow_switch:' in call.data:
        """
        Комада: follow_switch:<str>
        <str> - выбранная валюта
        Например: follow_switch:BTC
        Служит выбора/удаления валюты из списка отслеживаемых
        """
        # Парсим выбранную валюту
        currency = call.data.split(':')[1]
        # Получаем запись о пользователе из базы данных, если пользователь не существует, создаем его
        user, created = models.User.get_or_create(chat_id=call.message.chat.id)
        # Получаем список валют на которые подписан пользователь, преобразуем dict в list
        followed_currencies = list(json.loads(user.followed_currencies))
        # Если выбранная валюта находится в списке отсеживаемых то удаяляем ее
        if currency in followed_currencies:
            followed_currencies.remove(currency)
        # Иначе добавляем
        else:
            followed_currencies.append(currency)
        # Устанавливаем объекту пользователя JSON строку со списком отслеживаемых валют
        user.followed_currencies = json.dumps(followed_currencies)
        # Сохраняем в базу данных
        user.save()

        # Получаем список доступных валют
        crypto_s = get_available_currencies()

        # Создаем Inline клавиатуру
        markup = keyboards.keyboard_inline_checkbox_select_crypto(
            currencies=crypto_s,
            user_followed=followed_currencies)

        # Редактируем сообщение с клавиатурой, отправляя обновленную
        bot.edit_message_reply_markup(
            chat_id=call.message.chat.id,
            message_id=call.message.message_id,
            reply_markup=markup
        )

    if 'unfollow:' in call.data:
        # Парсим выбранную валюту
        currency = call.data.split(':')[1]
        # Получаем запись о пользователе из базы данных, если пользователь не существует, создаем его
        user, created = models.User.get_or_create(chat_id=call.message.chat.id)
        # Получаем список валют на которые подписан пользователь, преобразуем dict в list
        followed_currencies = list(json.loads(user.followed_currencies))
        # Если выбранная валюта находится в списке отсеживаемых то удаяляем ее
        if currency in followed_currencies:
            followed_currencies.remove(currency)
        # Устанавливаем объекту пользователя JSON строку со списком отслеживаемых валют
        user.followed_currencies = json.dumps(followed_currencies)
        # Сохраняем в базу данных
        user.save()
        # Проверяем, если список выбранных валют пуст то отправляем сообщение с необходимостью выбрать вылюты
        if not len(followed_currencies):
            msg_text = 'Для просмотра списка выбранных валют их сначала ' \
                       'нужно выбрать, нажмите /configure чтобы сделать это'
            bot.edit_message_text(
                message_id=call.message.message_id,
                chat_id=call.message.chat.id,
                text=msg_text,
            )
        # Иначе отправляем обновленную клавиатуру
        else:
            # Создаем Inline клавиатуру
            markup = keyboards.keyboard_inline_checkbox_select_crypto(user_followed=followed_currencies)
            # Редактируем сообщение с клавиатурой, отправляя обновленную
            bot.edit_message_reply_markup(
                chat_id=call.message.chat.id,
                message_id=call.message.message_id,
                reply_markup=markup
            )

    if 'base_select:' in call.data:
        """
        Комада: base_select:<str>
        <str> - выбранная валюта
        Например: base_select:BTC
        Служит выбора основной валюты
        """
        # Парсим выбранную валюту
        currency = call.data.split(':')[1]
        # Получаем запись о пользователе из базы данных, если пользователь не существует, создаем его
        user, created = models.User.get_or_create(chat_id=call.message.chat.id)
        # Устанавливаем основную валюту объекту пользователя
        user.base_currency = currency
        # Сохраняем его в базу данных
        user.save()
        # Редактируем сообщение с клавиатурой, отправляя текст с выбранной клавиатурой
        bot.edit_message_text(
            message_id=call.message.message_id,
            chat_id=call.message.chat.id,
            text='В качестве основной валюты выбрана: *{}*'.format(currency),
            parse_mode='MARKDOWN'
        )
        """
        Если список хэндлеров не пустой то нужно вызвать следующий,
        т.к. этот шаг был завершен ботом а не пользователем
        """
        if int(call.message.chat.id) in bot.next_step_handlers.keys():
            # Получаем список хэндлеров
            handlers = bot.next_step_handlers[call.message.chat.id]
            # Вызываем их по очереди
            for handler in handlers:
                # callback - метод хендлера
                # в качестве аргумента передается текущее сообщение
                handler.callback(call.message)
            # Очищаем список хэндлеров
            bot.clear_step_handler(call.message)

    if call.data == 'complete_following':
        """
        Служит для завершения выбора списка валют
        """
        # Получаем запись о пользователе из базы данных, если пользователь не существует, создаем его
        user, created = models.User.get_or_create(chat_id=call.message.chat.id)
        # Поолучаем список валют которые выбрал пользователь
        followed_currencies = json.loads(user.followed_currencies)
        # Если список валют не пуст, отправляем список с валютами на которые пользователь подписался
        if len(followed_currencies):
            msg_text = 'Вы подписались на следующие валюты: *{}*'.format(', '.join(followed_currencies))
        # Иначе что он ни на что не подписан
        else:
            msg_text = 'Вы отписались от всех валют'

        # Отправляем сообщение с текстом
        bot.edit_message_text(
            message_id=call.message.message_id,
            chat_id=call.message.chat.id,
            text=msg_text,
            parse_mode='MARKDOWN'
        )


def schedule():
    # Список пользователей которые уже получили уведомление
    while True:
        # Получаем текущее время
        now = datetime.datetime.now()
        # Получаем пользователей из базы по очереди у которых время создания записи в БД совпадае с текущим
        users = models.User.select().where(
            (models.User.joined_time.hour == now.hour) & (models.User.joined_time.minute == now.minute))
        # users = models.User.select().where(
        #     (models.User.joined_time.hour == now.hour) &
        #     (models.User.joined_time.minute == now.minute))
        for user in users:
            # Если пользователь получил уведомление переходим к следующему
            # if user.chat_id in notified_users:
            #     continue
            # Поолучаем список валют которые выбрал пользователь
            to_list = json.loads(user.followed_currencies)
            # Скливаем их в строку через запятую
            to = ','.join(to_list)
            # Получаем основную валюту пользователя
            _from = user.base_currency
            # Получаем цены на валюты
            response = requests.get(currencies_prices_url.format(_from, to)).json()
            # Формируем сообщение
            msg_text = '*Ежедневная рассылка:\n*'
            for price in response:
                msg_text += (
                    'Цена за 1 {}: *{:.8f}* {}\n'.format(price['from'], float(price['price']), price['to']))
            # Отправляем сообщение
            bot.send_message(user.chat_id, msg_text, parse_mode='MARKDOWN')
            # Добавляем пользователя в список получивших уведомление
            # notified_users.append(user.chat_id)
        # Засыпаем на 60 секунд чтобы не нагружать сервер
        sleep(60)


if __name__ == '__main__':
    # Создаем таблицы в базе данных
    models.create_tables()
    # Создаем поток процесса в котором работает планировщик уведомлений
    notifier = Process(target=schedule, args=())
    try:
        # Запускаем поток
        notifier.start()
    except:
        # При ошибке в потоке запускаем его заново
        notifier.start()
    # Запускаем бота
    bot.polling(none_stop=True)

