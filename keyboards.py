#! /usr/bin/env python
# -*- coding: utf-8 -*-


import math

from telebot import types


def keyboard_inline_checkbox_select_crypto(user_followed: list, currencies: list = None):
    # Создаем Inline клавиатуру с 3 элементами в одной строке
    keyboard = types.InlineKeyboardMarkup(row_width=3)
    # Пустой список кнопок, сюда будут добавлены кнопки с валютами
    buttons = []
    # Иконки для кнопок
    icons_checkbox = {'checked': '✅', 'unchecked': '☑️'}
    # Если список валют пуст то выводим валюты на которые подписан пользователь
    if currencies is None:
        for currency in user_followed:
            # Текст кнопки
            btn_text = '{} {}'.format(icons_checkbox['checked'], currency)
            # Создаем Inline кнопку
            btn = types.InlineKeyboardButton(
                text=btn_text,
                callback_data='unfollow:{}'.format(currency)
            )
            # Доабавляем ее в список с кнопками
            buttons.append(btn)
        # Добавляем все кнопки на клавиатуру
        keyboard.add(*buttons)
    # Иначе выводим список доступных валют
    else:
        for currency in currencies:
            # Если пользователь подписан на валюту то устанавливаем зеленую иконку для клавиатуры
            if currency in user_followed:
                btn_text = '{} {}'.format(icons_checkbox['checked'], currency)
            # Иначе серую
            else:
                btn_text = '{} {}'.format(icons_checkbox['unchecked'], currency)
            # Создаем Inline кнопку
            btn = types.InlineKeyboardButton(
                text=btn_text,
                callback_data='follow_switch:{}'.format(currency)
            )
            # Доабавляем ее в список с кнопками
            buttons.append(btn)
        # Создаем Inline кнопку для завершения выбора валют
        btn_complete_following = types.InlineKeyboardButton(
            text='💾 Завершить',
            callback_data='complete_following'
        )

        # Добавляем все кнопки на клавиатуру
        keyboard.add(*buttons)
        # Добавляем кнопку для завершения выбора валют на клавиатуру отдельной строкой, чтобы она не сливалась с остальными
        keyboard.row(btn_complete_following)
    # Возвращаем сгенерированную клавиатуру
    return keyboard


def keyboard_inline_radio_select_crypto(currencies: list, base_currency: str = None):
    # Создаем Inline клавиатуру с 3 элементами в одной строке
    keyboard = types.InlineKeyboardMarkup(row_width=3)
    # Пустой список кнопок, сюда будут добавлены кнопки с валютами
    buttons = []
    # Иконки для кнопок
    icons_radio = {'checked': '🔘', 'unchecked': '⚪️'}
    for currency in currencies:
        # Если пользователь подписан на валюту то устанавливаем зеленую иконку для клавиатуры
        if currency == base_currency:
            btn_text = '{} {}'.format(icons_radio['checked'], currency)
        # Иначе серую
        else:
            btn_text = '{} {}'.format(icons_radio['unchecked'], currency)
        # Создаем Inline кнопку
        btn = types.InlineKeyboardButton(
            text=btn_text,
            callback_data='base_select:{}'.format(currency)
        )
        # Доабавляем ее в список с кнопками
        buttons.append(btn)
    # Добавляем все кнопки на клавиатуру
    keyboard.add(*buttons)
    # Возвращаем сгенерированную клавиатуру
    return keyboard
