#! /usr/bin/env python
# -*- coding: utf-8 -*-


# Импортируем библиотеку для работы с БД
import datetime

from peewee import *

# Задаем имя файла для БД
DATABASE = 'db.sqlite'

# Инициализируем БД
db = SqliteDatabase(DATABASE)


class User(Model):
    """
    Модель пользователя

    chat_id - id чата с пользователем
    base_currency - валюта которуя пользователь выбрал по умолчания
    followed_currencies - список валют на которые подписан пользователь (по кмолчанию пустой массив JSON)
    joined_time - время создания записи в БД о пользователе
    """
    chat_id = IntegerField(unique=True)
    base_currency = CharField(default='', max_length=6, null=True)
    followed_currencies = TextField(default='[]', null=True)
    joined_time = DateTimeField(default=datetime.datetime.now())

    class Meta:
        # Meta класс с названием базы данных с которой будет связана модель
        database = db


def create_tables():
    with db:
        # Создаем таблицы в БД
        db.create_tables([User])
